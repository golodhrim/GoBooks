<?xml version='1.0' encoding='utf-8'?>
<package xmlns="http://www.idpf.org/2007/opf" unique-identifier="uuid_id" version="2.0">
    <metadata xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:opf="http://www.idpf.org/2007/opf">
        <dc:identifier opf:scheme="calibre" id="calibre_id">362</dc:identifier>
        <dc:identifier opf:scheme="uuid" id="uuid_id">ae50221d-21fc-4a8a-98cd-9ce5c5f05d0c</dc:identifier>
        <dc:title>Building Microservices With Go</dc:title>
        <dc:creator opf:file-as="Jackson, Nic" opf:role="aut">Nic Jackson</dc:creator>
        <dc:contributor opf:file-as="calibre" opf:role="bkp">calibre (5.26.0) [https://calibre-ebook.com]</dc:contributor>
        <dc:date>2017-07-27T19:32:09+00:00</dc:date>
        <dc:description>Your one-stop guide to the common patterns and practices, showing you how to apply these using the Go programming languageAbout This BookThis short, concise, and practical guide is packed with real-world examples of building microservices with GoIt is easy to read and will benefit smaller teams who want to extend the functionality of their existing systemsUsing this practical approach will save your money in terms of maintaining a monolithic architecture and demonstrate capabilities in ease of useWho This Book Is ForYou should have a working knowledge of programming in Go, including writing and compiling basic applications. However, no knowledge of RESTful architecture, microservices, or web services is expected. If you are looking to apply techniques to your own projects, taking your first steps into microservice architecture, this book is for you.What You Will LearnPlan a microservice architecture and design a microserviceWrite a microservice with a RESTful API and a databaseUnderstand the common idioms and common patterns in microservices architectureLeverage tools and automation that helps microservices become horizontally scalableGet a grounding in containerization with Docker and Docker-Compose, which will greatly accelerate your development lifecycleManage and secure Microservices at scale with monitoring, logging, service discovery, and automationTest microservices and integrate API tests in GoIn DetailMicroservice architecture is sweeping the world as the de facto pattern to build web-based applications. Golang is a language particularly well suited to building them. Its strong community, encouragement of idiomatic style, and statically-linked binary artifacts make integrating it with other technologies and managing microservices at scale consistent and intuitive. This book will teach you the common patterns and practices, showing you how to apply these using the Go programming language.It will teach you the fundamental concepts of architectural design and RESTful communication, and show you patterns that provide manageable code that is supportable in development and at scale in production. We will provide you with examples on how to put these concepts and patterns into practice with Go.Whether you are planning a new application or working in an existing monolith, this book will explain and illustrate with practical examples how teams of all sizes can start solving problems with microservices. It will help you understand Docker and Docker-Compose and how it can be used to isolate microservice dependencies and build environments. We finish off by showing you various techniques to monitor, test, and secure your microservices.By the end, you will know the benefits of system resilience of a microservice and the advantages of Go stack.Style and approachThe step-by-step tutorial focuses on building microservices. Each chapter expands upon the previous one, teaching you the main skills and techniques required to be a successful microservice practitioner.</dc:description>
        <dc:publisher>Packt Publishing Ltd</dc:publisher>
        <dc:identifier opf:scheme="GOOGLE">B-dDDwAAQBAJ</dc:identifier>
        <dc:identifier opf:scheme="ISBN">9781786469793</dc:identifier>
        <dc:language>eng</dc:language>
        <dc:subject>Web</dc:subject>
        <dc:subject>Java</dc:subject>
        <dc:subject>Programming Languages</dc:subject>
        <dc:subject>Computers</dc:subject>
        <dc:subject>COM060080 - COMPUTERS / Web / General</dc:subject>
        <dc:subject>General</dc:subject>
        <dc:subject>COM051010 - COMPUTERS / Programming Languages / General</dc:subject>
        <dc:subject>COM051280 - COMPUTERS / Programming Languages / Java</dc:subject>
        <meta name="calibre:author_link_map" content="{&quot;Nic Jackson&quot;: &quot;&quot;}"/>
        <meta name="calibre:timestamp" content="2019-12-01T19:19:36+00:00"/>
        <meta name="calibre:title_sort" content="Building Microservices With Go"/>
    </metadata>
    <guide>
        <reference type="cover" title="Cover" href="Building Microservices With Go - Nic Jackson.jpg"/>
    </guide>
</package>
